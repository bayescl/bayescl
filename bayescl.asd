;; $Id: bayescl.asd,v 1.1.1.1 2003/11/25 15:55:34 aventimiglia Exp $
;;
;; bayescl - A Common Lisp Bayesian Filtering Library
;; (c) 2003 Anthony J Ventimiglia.
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the terms of the Lisp Lesser GNU
;; Public License, known as the LLGPL
;; 
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
;;
;; You should have received a copy of the Lisp Lesser General Public
;; License along with this library; if not, please see
;; http://opensource.franz.com/preamble.html.
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/bayescl


(in-package :asdf)

(defsystem #:bayescl
  :name "net.common-lisp.bayescl"
  :author "Anthony Ventimiglia <aventimiglia@common-lisp.net>"
  :maintainer "Anthony Ventimiglia <aventimiglia@common-lisp.net>"
  :version #.(or nil
		 (let* ((trimmed
			 (string-trim
			  "$ Date:" "$Date: 2003/11/25 15:55:34 $"))
			(date (subseq trimmed 0 (search " " trimmed))))
		   (concatenate 'string (subseq date 0 4)
				(subseq date 5 7) (subseq date 8 10) "cvs")))
  
  :description "A Package for Beysian Pattern Filtering"
  :long-description "This has to be worked on"
  :components
  ((:file "package")
   (:file "bucket" :depends-on ("package"))
   (:file "filter" :depends-on ("bucket"))))
   