;; $Id$
;;
;; bayescl - A Common Lisp Bayesian Filtering Library
;; (c) 2003 Anthony J Ventimiglia.
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the terms of the Lisp Lesser GNU
;; Public License, known as the LLGPL
;; 
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
;;
;; You should have received a copy of the Lisp Lesser General Public
;; License along with this library; if not, please see
;; http://opensource.franz.com/preamble.html.
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/bayescl

(in-package #:cl-user)

(defpackage :net.common-lisp.bayescl
  (:nicknames #:bayescl #:bayes)
  (:use #:cl)
  (:export #:make-bucket #:bucket-name #:add-token #:score-token #:save-bucket
	   #:load-bucket #:make-filter #:*minimum-score*))