;; $Id$
;;
;; bayescl - A Common Lisp Bayesian Filtering Library
;; (c) 2003 Anthony J Ventimiglia.
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the terms of the Lisp Lesser GNU
;; Public License, known as the LLGPL
;; 
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
;;
;; You should have received a copy of the Lisp Lesser General Public
;; License along with this library; if not, please see
;; http://opensource.franz.com/preamble.html.
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/bayescl

(in-package :net.common-lisp.bayescl)

(defclass filter ()
  ((buckets :accessor buckets
	    :type list
	    :initarg :buckets
	    :documentation "A list of buckets used for filtering")))

(defun make-filter (buckets)
  (if (< (length buckets) 2) 'error
    (make-instance 'filter :buckets buckets)))

(defmethod score-token ((self filter) token)
  "Returns an a list of scores for each bucket in the filter"
  (let ((bucket-ct (length (buckets self))))
    (let (; Prior probabilities
	  (P[Hx] (make-array bucket-ct	
			       :element-type 'rational
			       :initial-element (/ 1 bucket-ct)))
	  ; Conditional probabilities
	  (P[D/Hx] (make-array bucket-ct :element-type 'rational
			       :initial-contents
			       (loop for bucket in (buckets self)
				     collect (score-token bucket token)))))
      (let ((dividend (apply #'+
			     (loop for i from 0 to (1- bucket-ct)
				   collect (* (aref P[Hx] i)
					      (aref P[D/Hx] i))))))
	;; Return all posterior probabilities
	(loop for i from 0 to (1- bucket-ct)
	      collect (if (zerop dividend) *minimum-score*
			  (/ (* (aref P[Hx] i) (aref P[D/Hx] i))
			     dividend)))))))

   